import org.junit.Assert;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.time.Duration;

public class MySeleniumTest {

    private WebDriver driver;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Java\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void checkTotalPrice() throws InterruptedException{
        driver.get("https://www.automationtesting.co.uk/");
        String pageTitle = driver.getTitle();
        Assert.assertEquals("Homepage", pageTitle);

        driver.findElement(By.cssSelector(".close-cookie-warning>span")).click();
        driver.findElement(By.cssSelector(".toggle")).click();
        WebElement testStore = driver.findElement(By.linkText("TEST STORE"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", testStore);
        testStore.click();
        Thread.sleep(500);

        pageTitle = driver.getTitle();
        Assert.assertEquals("teststore", pageTitle);
        WebElement hummingBirdTshirt = driver.findElement(By.linkText("Hummingbird Printed T-Shirt"));
        js.executeScript("arguments[0].scrollIntoView(true)", hummingBirdTshirt);
        hummingBirdTshirt.click();
        Thread.sleep(500);

        pageTitle = driver.getTitle();
        Assert.assertEquals("Hummingbird printed t-shirt", pageTitle);
        WebElement addToCartBtn = driver.findElement(By.cssSelector(".add-to-cart"));
        js.executeScript("arguments[0].scrollIntoView(true)", addToCartBtn);
        addToCartBtn.click();
        Thread.sleep(1000);

        String totalPrice = driver.findElement(By.cssSelector(".product-total > span.value")).getText();
        Assert.assertEquals("$26.12", totalPrice);
    }

    @Test
    public void explicitWait(){
        driver.get("https://www.automationtesting.co.uk/loader.html");
        String pageTitle = driver.getTitle();
        Assert.assertEquals("Loader", pageTitle);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button#loaderBtn")));
        driver.findElement(By.cssSelector("button#loaderBtn")).click();

    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
